class CfgWeapons {
  class CUP_V_B_BAF_DDPM_Osprey_Mk3_AutomaticRifleman {
    class ItemInfo;
  };

  class CUP_V_B_BAF_DDPM_Osprey_Mk3_Grenadier {
    class ItemInfo;
  };

  class CUP_V_B_BAF_DDPM_Osprey_Mk3_Medic {
    class ItemInfo;
  };

  class CUP_V_B_BAF_DDPM_Osprey_Mk3_Officer {
    class ItemInfo;
  };

  class CUP_V_B_BAF_DDPM_Osprey_Mk3_Rifleman {
    class ItemInfo;
  };
  class pvm_rm_mg: CUP_V_B_BAF_DDPM_Osprey_Mk3_AutomaticRifleman {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 4;
    };
    class ItemInfo :ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] Royal Marines MG(Desert)";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_rm_gren: CUP_V_B_BAF_DDPM_Osprey_Mk3_Grenadier {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 6;
      grenades = 4;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] Royal Marines Grenadier(Desert)";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_rm_medic: CUP_V_B_BAF_DDPM_Osprey_Mk3_Medic {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 2;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] Royal Marines Medic(Desert)";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_rm_off: CUP_V_B_BAF_DDPM_Osprey_Mk3_Officer {
    class VTN_VESTINFO {
      primarymagazines[] = {5, 45};
      handgunmagazines[] = {5, 20};
      uglmagazines = 0;
      grenades = 2;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] Royal Marines TL/SL(Desert)";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_rm_rifleman: CUP_V_B_BAF_DDPM_Osprey_Mk3_Rifleman {
    class VTN_VESTINFO {
      primarymagazines[] = {9, 45};
      handgunmagazines[] = {3, 20};
      uglmagazines = 0;
      grenades = 3;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] Royal Marines Rifleman(Desert)";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };
};
