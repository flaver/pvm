// Register keybinding
#include "script_component.hpp"
#include "\a3\editor_f\Data\Scripts\dikCodes.h"

if (!hasInterface) exitWith {};

[
  "Extended Veteran Mod",
  QGVAR(DisplayAmmoCount),
  "Display Ammo Count",
  {
    [player] call FUNC(displayAmmoCount);
  },
  "",
  [DIK_R, [false, true, false]]
] call CBA_fnc_addKeybind;
