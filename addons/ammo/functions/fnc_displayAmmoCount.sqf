params ["_player"];

_weapon = currentWeapon _player;

_count _player ammo _weapon;

hint format["Ammo: %1", _count];
