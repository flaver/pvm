#define COMPONENT uniform_gen
#include "\z\pvm\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_UNIFORM_GEN
    #define DEBUG_MODE_FULL
#endif
    #ifdef DEBUG_SETTINGS_UNIFORM_GEN
    #define DEBUG_SETTINGS DEBUG_SETTINGS_UNIFORM_GEN
#endif

#include "\z\pvm\addons\main\script_macros.hpp"
