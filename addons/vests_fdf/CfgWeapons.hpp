class CfgWeapons {
  class FDF_VEST_21 {
    class ItemInfo;
  };

  class FDF_VEST_22 {
    class ItemInfo;
  };

  class milgp_v_mmac_teamleader_belt_MC;
  class milgp_v_mmac_teamleader_MC;
  class milgp_v_mmac_teamleader_rgr;
  class milgp_v_mmac_teamleader_belt_rgr;

  class milgp_v_mmac_medic_belt_MC;
  class milgp_v_mmac_medic_MC;
  class milgp_v_mmac_medic_belt_rgr;
  class milgp_v_mmac_medic_rgr;

  class milgp_v_mmac_marksman_belt_MC;

  class milgp_v_mmac_hgunner_belt_MC;
  class milgp_v_mmac_hgunner_MC;
  class milgp_v_mmac_hgunner_rgr;
  class milgp_v_mmac_hgunner_belt_rgr;

  class milgp_v_mmac_grenadier_belt_MC;

  class milgp_v_mmac_assaulter_belt_MC;
  class milgp_v_mmac_assaulter_MC;
  class milgp_v_mmac_assaulter_belt_rgr;
  class milgp_v_mmac_assaulter_rgr;

  class milgp_v_battle_belt_assaulter_RGR;
  class milgp_v_battle_belt_hgunner_KHK;

  class milgp_v_jpc_assaulter_khk;
  class milgp_v_jpc_assaulter_rgr;
  class milgp_v_jpc_assaulter_belt_khk;
  class milgp_v_jpc_assaulter_belt_rgr;

  class milgp_v_jpc_Grenadier_khk;
  class milgp_v_jpc_Grenadier_rgr;
  class milgp_v_jpc_Grenadier_belt_khk;
  class milgp_v_jpc_Grenadier_belt_rgr;

  class milgp_v_jpc_hgunner_khk;
  class milgp_v_jpc_hgunner_rgr;
  class milgp_v_jpc_hgunner_belt_khk;
  class milgp_v_jpc_hgunner_belt_rgr;

  class milgp_v_jpc_Light_khk;
  class milgp_v_jpc_Light_rgr;

  class milgp_v_jpc_Marksman_khk;
  class milgp_v_jpc_Marksman_rgr;
  class milgp_v_jpc_marksman_belt_khk;
  class milgp_v_jpc_marksman_belt_rgr;

  class milgp_v_jpc_Medic_khk;
  class milgp_v_jpc_Medic_rgr;
  class milgp_v_jpc_medic_belt_khk;
  class milgp_v_jpc_medic_belt_rgr;

  class milgp_v_jpc_TeamLeader_khk;
  class milgp_v_jpc_TeamLeader_rgr;
  class milgp_v_jpc_teamleader_belt_khk;
  class milgp_v_jpc_teamleader_belt_rgr;

  class milgp_v_mmac_assaulter_KHK;
  class milgp_v_mmac_assaulter_rgr;
  class milgp_v_mmac_assaulter_belt_KHK;
  class milgp_v_mmac_assaulter_belt_rgr;

  class milgp_v_mmac_grenadier_KHK;
  class milgp_v_mmac_grenadier_rgr;
  class milgp_v_mmac_grenadier_belt_KHK;
  class milgp_v_mmac_grenadier_belt_rgr;

  class milgp_v_mmac_hgunner_KHK;
  class milgp_v_mmac_hgunner_rgr;
  class milgp_v_mmac_hgunner_belt_KHK;
  class milgp_v_mmac_hgunner_belt_rgr;

  class milgp_v_mmac_marksman_KHK;
  class milgp_v_mmac_marksman_rgr;
  class milgp_v_mmac_marksman_belt_KHK;
  class milgp_v_mmac_marksman_belt_rgr;

  class milgp_v_mmac_medic_KHK;
  class milgp_v_mmac_medic_rgr;
  class milgp_v_mmac_medic_belt_KHK;
  class milgp_v_mmac_medic_belt_rgr;

  class milgp_v_mmac_teamleader_KHK;
  class milgp_v_mmac_teamleader_rgr;
  class milgp_v_mmac_teamleader_belt_KHK;
  class milgp_v_mmac_teamleader_belt_rgr;

  class V_TacChestrig_grn_F;

  class pvm_fdf_d_light: FDF_VEST_21 {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 4;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras FDF Light Vest (Sand)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  //VEST FOR MG
  class pvm_fdf_d_heavy: FDF_VEST_22 {
    class VTN_VESTINFO {
      primarymagazines[] = {2, 270};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 4;
    };
    class ItemInfo :ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras FDF Heavy Vest (Sand)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  // VEST FOR GL
  class pvm_fdf_d_light_gl :FDF_VEST_21 {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 10;
      grenades = 0;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras FDF Light Vest GL (Sand)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  // Vests for FRDF
  class pvm_frdf_mc_teamleader: milgp_v_mmac_teamleader_belt_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 7;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Teamleader (MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_mc_teamleader_no_belt: milgp_v_mmac_teamleader_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 3;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Teamleader No Belt (MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_frdf_rgr_teamleader: milgp_v_mmac_teamleader_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 7;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Teamleader (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_rgr_teamleader_no_belt: milgp_v_mmac_teamleader_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 3;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Teamleader No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_frdf_mc_medic: milgp_v_mmac_medic_belt_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Medic (MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_mc_medic_no_belt: milgp_v_mmac_medic_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Medic No Belt (MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_rgr_medic: milgp_v_mmac_medic_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Medic (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_rgr_medic_no_belt: milgp_v_mmac_medic_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Medic No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_frdf_mc_marksman: milgp_v_mmac_marksman_belt_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Marksman (MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_frdf_mc_mg: milgp_v_mmac_hgunner_belt_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Machine Gunner (MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_mc_mg_no_belt: milgp_v_mmac_hgunner_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Machine Gunner No Belt(MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_rgr_mg: milgp_v_mmac_hgunner_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Machine Gunner (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_mc_mg_no_belt: milgp_v_mmac_hgunner_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Machine Gunner No Belt(RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_frdf_mc_grenadier: milgp_v_mmac_grenadier_belt_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 10;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Grenadier (MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };

  class pvm_frdf_mc_rifleman: milgp_v_mmac_assaulter_belt_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Rifleman (MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_mc_rifleman_no_belt: milgp_v_mmac_assaulter_MC {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Rifleman No Belt(MC)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_rgr_rifleman: milgp_v_mmac_assaulter_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Rifleman (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_frdf_mc_rifleman_no_belt: milgp_v_mmac_assaulter_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] MarCiras Rifleman No Belt(RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_battle_belt_assaulter_RGR: milgp_v_battle_belt_assaulter_RGR {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 0;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] Belt(RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_battle_belt_hgunner_KHK: milgp_v_battle_belt_hgunner_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 0;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] Belt(KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_assaulter_khk: milgp_v_jpc_assaulter_khk {
   class VTN_VESTINFO {
      primarymagazines[] = {5, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] JPC Rifleman No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_assaulter_rgr: milgp_v_jpc_assaulter_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {5, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] JPC Rifleman No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_assaulter_belt_khk :milgp_v_jpc_assaulter_belt_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {9, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] JPC Rifleman (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_assaulter_belt_rgr :milgp_v_jpc_assaulter_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {9, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] JPC Rifleman (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_Grenadier_khk :milgp_v_jpc_Grenadier_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {5, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 4;
      grenades = 0;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] JPC Grenadier No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_Grenadier_khk :milgp_v_jpc_Grenadier_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {5, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 4;
      grenades = 0;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] JPC Grenadier No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_Grenadier_rgr :milgp_v_jpc_Grenadier_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {5, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 4;
      grenades = 0;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] JPC Grenadier No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_Grenadier_belt_khk :milgp_v_jpc_Grenadier_belt_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {9, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 10;
      grenades = 0;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] JPC Grenadier (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_Grenadier_belt_rgr :milgp_v_jpc_Grenadier_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {9, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 10;
      grenades = 0;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] JPC Grenadier (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_hgunner_khk :milgp_v_jpc_hgunner_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Machine Gunner No Belt(KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_hgunner_rgr :milgp_v_jpc_hgunner_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Machine Gunner No Belt(RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_hgunner_belt_khk :milgp_v_jpc_hgunner_belt_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Machine Gunner (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_hgunner_belt_rgr :milgp_v_jpc_hgunner_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Machine Gunner (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_Light_khkr :milgp_v_jpc_Light_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 45};
      handgunmagazines[] = {0, 00};
      uglmagazines = 0;
      grenades = 0;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Light No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_Light_rgr :milgp_v_jpc_Light_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 45};
      handgunmagazines[] = {0, 00};
      uglmagazines = 0;
      grenades = 0;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Light No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_Marksman_khk :milgp_v_jpc_Marksman_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Marksman No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_Marksman_rgr :milgp_v_jpc_Marksman_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Marksman No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_marksman_belt_khk :milgp_v_jpc_marksman_belt_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Marksman (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_marksman_belt_rgr :milgp_v_jpc_marksman_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Marksman (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_v_jpc_Medic_khk :milgp_v_jpc_Medic_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Medic No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_v_jpc_Medic_rgr :milgp_v_jpc_Medic_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Medic No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_medic_belt_khk :milgp_v_jpc_medic_belt_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {7, 45};
      handgunmagazines[] = {1, 1};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Medic (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
 class pvm_jpc_medic_belt_rgr :milgp_v_jpc_medic_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {7, 45};
      handgunmagazines[] = {1, 1};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Medic (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_TeamLeader_khk :milgp_v_jpc_TeamLeader_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Teamleader No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_TeamLeader_rgr :milgp_v_jpc_TeamLeader_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Teamleader No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_teamleader_belt_khk :milgp_v_jpc_teamleader_belt_khk {
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 4;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Teamleader (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_jpc_teamleader_belt_rgr :milgp_v_jpc_teamleader_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 4;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] JPC Teamleader (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_assaulter_KHK :milgp_v_mmac_assaulter_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Rifleman No Belt(KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_assaulter_rgr :milgp_v_mmac_assaulter_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Rifleman No Belt(RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_assaulter_belt_khk :milgp_v_mmac_assaulter_belt_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Rifleman (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_assaulter_belt_rgr :milgp_v_mmac_assaulter_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Rifleman (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_grenadier_KHK :milgp_v_mmac_grenadier_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 4;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Grenadier No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_grenadier_rgr :milgp_v_mmac_grenadier_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 4;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Grenadier No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_grenadier_belt_KHK :milgp_v_mmac_grenadier_belt_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 10;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Grenadier (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_grenadier_belt_rgr :milgp_v_mmac_grenadier_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 10;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Grenadier (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_hgunner_KHK :milgp_v_mmac_hgunner_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Machine Gunner No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_hgunner_rgr :milgp_v_mmac_hgunner_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {3, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Machine Gunner No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_hgunner_belt_KHK :milgp_v_mmac_hgunner_belt_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Machine Gunner (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_hgunner_belt_rgr :milgp_v_mmac_hgunner_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 270};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Machine Gunner (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_marksman_KHK :milgp_v_mmac_marksman_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Marksman No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_marksman_rgr :milgp_v_mmac_marksman_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Marksman No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_marksman_belt_KHK :milgp_v_mmac_marksman_belt_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Marksman (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_marksman_belt_rgr :milgp_v_mmac_marksman_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {10, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Marksman (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_medic_KHK :milgp_v_mmac_medic_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {5, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Medic No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_medic_rgr :milgp_v_mmac_medic_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {5, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Medic No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_medic_belt_KHK :milgp_v_mmac_medic_belt_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {9, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Medic (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_medic_belt_rgr :milgp_v_mmac_medic_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {9, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Medic (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_teamleader_KHK :milgp_v_mmac_teamleader_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 3;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Teamleader No Belt (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_teamleader_rgr :milgp_v_mmac_teamleader_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 3;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Teamleader No Belt (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_teamleader_belt_KHK :milgp_v_mmac_teamleader_belt_KHK {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 7;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Teamleader (KHK)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_mmac_teamleader_belt_rgr :milgp_v_mmac_teamleader_belt_rgr {
    class VTN_VESTINFO {
      primarymagazines[] = {6, 45};
      handgunmagazines[] = {1, 20};
      uglmagazines = 7;
      grenades = 1;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] MMAC Teamleader (RGR)";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
  class pvm_V_TacChestrig_grn_F :V_TacChestrig_grn_F {
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 0;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] Tactical Cheastrig";
    scope=2;
    author="Adacas + [Pori Brig.] flaver";
  };
};
