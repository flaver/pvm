#define COMPONENT vests_fdf
#include "\z\pvm\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_VESTS_FDF
    #define DEBUG_MODE_FULL
#endif
    #ifdef DEBUG_SETTINGS_VESTS_FDF
    #define DEBUG_SETTINGS DEBUG_SETTINGS_VESTS_FDF
#endif

#include "\z\pvm\addons\main\script_macros.hpp"
