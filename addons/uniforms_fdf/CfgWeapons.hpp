class CfgWeapons {
  // Externals
  class FDF_fat_m04_helle_sin_2 {
    class ItemInfo;
  };

  class FDF_fat_rs_m04_helle_tac_1 {
    class ItemInfo;
  };

  class FDF_fat_m05_pakkas_tac_2 {
    class ItemInfo;
  };

  class FDF_fat_m05_lumi_sin_1 {
    class ItemInfo;
  };

  class FDF_fat_m05_maasto_sin_2 {
    class ItemInfo;
  };

  class FDF_fat_rs_m05_maasto_sin_2 {
    class ItemInfo;
  };

  class pvm_fdf_d :FDF_fat_m04_helle_sin_2 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M04 Arid";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_rs_d :FDF_fat_rs_m04_helle_tac_1 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M04 Arid (Rolled Up)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_pakkas :FDF_fat_m05_pakkas_tac_2 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M05 Cold Weather";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_lumi :FDF_fat_m05_lumi_sin_1 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M05 Snow";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_maasto :FDF_fat_m05_maasto_sin_2 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
      // Change display name etc, just to find it better
    displayName = "[PB] FDF M05 New";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };

  class pvm_fdf_rs_maasto :FDF_fat_rs_m05_maasto_sin_2 {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] FDF M05 New (Rolled Up)";
    scope=2;
    author="Ianassa + [Pori Brig.] flaver";
  };
};
