#define COMPONENT uniforms_uscm
#include "\z\pvm\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_UNIFORMS_USCM
    #define DEBUG_MODE_FULL
#endif
    #ifdef DEBUG_SETTINGS_UNIFORMS_USCM
    #define DEBUG_SETTINGS DEBUG_SETTINGS_UNIFORMS_USCM
#endif

#include "\z\pvm\addons\main\script_macros.hpp"
