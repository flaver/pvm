class CfgWeapons {
  // Externals

  class CUP_U_B_USMC_FROG4_WMARPAT {
    class ItemInfo;
  };

  class CUP_U_B_USMC_FROG4_DMARPAT {
    class ItemInfo;
  };

  class CUP_U_B_USMC_PilotOverall {
    class ItemInfo;
  };

  class pvm_usmc_w: CUP_U_B_USMC_FROG4_WMARPAT {
    // VT-Mod comp
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
      // Change display name etc, just to find it better
      displayName = "[PB] USMC (Woodland)";
      scope=2;
      author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_usmc_d: CUP_U_B_USMC_FROG4_DMARPAT {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
      // Change display name etc, just to find it better
      displayName = "[PB] USMC (Desert)";
      scope=2;
      author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_usmc_pilot: CUP_U_B_USMC_PilotOverall {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] USMC (Pilot)";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

};
