#include "script_component.hpp"
class CfgPatches {
  class ADDON {
      name = COMPONENT;
      units[] = {};
      weapons[] = {};
      requiredVersion = REQUIRED_VERSION;
      requiredAddons[] = {};
      author = "flaver,Realeasy";
      VERSION_CONFIG;
  };
};


#include "CfgEventHandlers.hpp"
#include "CfgWeapons.hpp"
