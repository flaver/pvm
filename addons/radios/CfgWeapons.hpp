class CfgWeapons {
  class ACRE_PRC117F;
  class CBA_MiscItem_ItemInfo;

  class pb_ACRE_PRC117F: ACRE_PRC117F {
    class ItemInfo: CBA_MiscItem_ItemInfo {
      allowedSlots[] = {619,701,801,901};
      mass= 1;
      scope = 0;
    };
    displayName="[PB] PRC117";
    author="[Pori Brig.] flaver + ACRE-Team"
  };

};
