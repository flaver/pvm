#include "script_component.hpp"
class CfgPatches {
  class ADDON {
    name = COMPONENT;
    units[] = {};
    weapons[] = {"pb_ACRE_PRC117F"};
    requiredVersion = REQUIRED_VERSION;
    requiredAddons[] = {};
    author = "flaver";
    VERSION_CONFIG;
  };
};


#include "CfgEventHandlers.hpp"
#include "CfgWeapons.hpp"
