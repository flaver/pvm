class CfgWeapons {
  class CUP_V_B_Eagle_SPC_Rifleman {
    class ItemInfo;
  };

  class CUP_V_B_Eagle_SPC_MG {
    class ItemInfo;
  };

  class CUP_V_B_Eagle_SPC_GL {
    class ItemInfo;
  };

  class CUP_V_B_Eagle_SPC_TL {
    class ItemInfo;
  };

  class CUP_V_B_Eagle_SPC_SL {
    class ItemInfo;
  };
  class CUP_V_B_PilotVest {
    class ItemInfo;
  };
  class pvm_usmc_rfl: CUP_V_B_Eagle_SPC_Rifleman {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 4;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Rifleman";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_usmc_mg: CUP_V_B_Eagle_SPC_MG {
    class VTN_VESTINFO {
      primarymagazines[] = {2, 270};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 4;
    };
    class ItemInfo :ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC MG";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_usmc_gl: CUP_V_B_Eagle_SPC_GL {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 6;
      grenades = 3;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Grenadier";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_usmc_tl: CUP_V_B_Eagle_SPC_TL {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {4, 45};
      handgunmagazines[] = {2, 20};
      uglmagazines = 0;
      grenades = 2;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Team Leader";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_usmc_sl: CUP_V_B_Eagle_SPC_SL {
    // VT-Mod comp
    class VTN_VESTINFO {
      primarymagazines[] = {8, 45};
      handgunmagazines[] = {0, 0};
      uglmagazines = 0;
      grenades = 2;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };

    // Change display name etc, just to find it better
    displayName = "[PB] USMC Squad Leader";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };
  class pvm_usmc_pilot_vest: CUP_V_B_PilotVest {
    class VTN_VESTINFO {
      primarymagazines[] = {0, 0};
      handgunmagazines[] = {1, 20};
      uglmagazines = 0;
      grenades = 2;
    };
    class ItemInfo: ItemInfo {
      armor = 30;
    };
    // Change display name etc, just to find it better
    displayName = "[PB] USMC Pilot";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };
};
