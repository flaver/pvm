#define COMPONENT uniforms_baf
#include "\z\pvm\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_UNIFORMS_BAF
    #define DEBUG_MODE_FULL
#endif
    #ifdef DEBUG_SETTINGS_UNIFORMS_BAF
    #define DEBUG_SETTINGS DEBUG_SETTINGS_UNIFORMS_BAF
#endif

#include "\z\pvm\addons\main\script_macros.hpp"
