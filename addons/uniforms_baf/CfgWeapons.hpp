class CfgWeapons {
  // Externals
  class CUP_U_B_BAF_DDPM_UBACSROLLEDKNEE {
    class ItemInfo;
  };

  class CUP_U_B_BAF_DDPM_UBACSLONGKNEE {
    class ItemInfo;
  };

  class pvm_rm_d_r: CUP_U_B_BAF_DDPM_UBACSROLLEDKNEE {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] RM-Rolled Up (Desert)";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };

  class pvm_rm_d: CUP_U_B_BAF_DDPM_UBACSLONGKNEE {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    // Change display name etc, just to find it better
    displayName = "[PB] RM Up (Desert)";
    scope=2;
    author="CUP Team + [Pori Brig.] flaver";
  };
};
