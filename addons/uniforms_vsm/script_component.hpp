#define COMPONENT uniforms_vsm
#include "\z\pvm\addons\main\script_mod.hpp"

// #define DEBUG_MODE_FULL
// #define DISABLE_COMPILE_CACHE

#ifdef DEBUG_ENABLED_UNIFORMS_VSM
    #define DEBUG_MODE_FULL
#endif
    #ifdef DEBUG_SETTINGS_UNIFORMS_VSM
    #define DEBUG_SETTINGS DEBUG_SETTINGS_UNIFORMS_VSM
#endif

#include "\z\pvm\addons\main\script_macros.hpp"
