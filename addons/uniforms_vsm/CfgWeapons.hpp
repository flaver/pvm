class CfgWeapons {
  // Externals
  class VSM_Multicam_Crye_SS_Camo {
    class ItemInfo;
  };

  class pvm_vms_multicam: VSM_Multicam_Crye_SS_Camo {
    class ItemInfo: ItemInfo {
      containerClass="Supply0";
    };
    displayName = "[PB] Multicam";
    scope=2;
    author="VSM + [Pori Brig.] flaver";
  };

};
