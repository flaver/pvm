class CfgVehicles {

	class ffp_rg32m;
    class ffp_rg32m_gmg;
    class rhsusf_M1232_MK19_usarmy_wd;
    class rhsusf_M1232_M2_usarmy_wd;

	class pvm_ffp_rg32m: ffp_rg32m {
		displayName="[PB] RG32m";
		class AcreIntercoms {
			class Intercom_1 {
                displayName = "Crew intercom";             // Name of the intercom network displayed to the players
                shortName = "Crew";                        // Short name of the intercom network. Maximum of 5 characters
                // Seats with stations configured that have intercom access. In this case, units in commander, driver, gunner and turret (excluding FFV) have access to this intercom
                // If left empty it has the same effect
                allowedPositions[] = {"all"};
                // In this case the commander turret does not have access to crew intercom (unit is "turned out"). This can be useful for historical vehicles (default: {})
                disabledPositions[] = {};
                // Despite not having regular access to the network, units in cargo positions can have limited connections to communicate with the crew. These positions do not transmit automatically in the limited network; units in this position must toggle the functionality manually. (default: {})
                limitedPositions[] = {};
                // This is the number of simultaneous connections that units defined in the previous array can have (default: 0)
                numLimitedPositions = 0;
                // Seats with master stations have the possibility of broadcasting a message in that network (default: {})
                masterPositions[] = {};
                // The intercom initial configuration is enabled upon entering a vehicle (default: 0)
                connectedByDefault = 1;
            };
		};

        class AcreRacks {
            class Rack_1 {
                displayName = "Dashboard Upper";      // Name displayed in the interaction menu
                shortName = "D.Up";                   // Short name displayed on the HUD. Maximum of 5 characters
                componentName = "ACRE_VRC110";        // Able to mount a PRC152
                allowedPositions[] = {"driver", {"cargo", 1}}; // Who can configure the radio and open the radio GUI. Same wildcards as the intercom. It also allows transmitting/receiving
                disabledPositions[] = {};             // Who cannot access the radio (default: {})
                defaultComponents[] = {};             // Use this to attach simple components like Antennas, they will first attempt to fill empty connectors but will overide existing connectors. Not yet fully implemented. (default: {})
                mountedRadio = "";                    // Predefined mounted radio (default: "", meaning none)
                isRadioRemovable = 1;                 // Radio can be removed (default: 0)
                intercom[] = {};                      // Radio not wired to any intercom. All units in intercom can receive/send transmittions (ACE3 interaction menu) but they cannot manipulate the radio (GUI interface) (default: {})
            };
            class Rack_2 {
                displayName = "Dashboard Lower";      // Name displayed in the interaction menu
                shortName = "D.Low";                  // Short name displayed on the HUD. Maximum of 5 characters
                componentName = "ACRE_VRC103";        // Rack type (able to mount a PRC117F)
                allowedPositions[] = {"driver", "commander", "gunner"}; // Who can configure the radio and open the radio GUI. Same wildcards as the intercom. It also allows transmitting/receiving
                disabledPositions[] = {};
                defaultComponents[] = {};
                mountedRadio = "ACRE_PRC117F";        // Predefined mounted radio
                isRadioRemovable = 0;                 // Radio cannot be removed
                intercom[] = {};                      // Radio not wired to any intercom. All units in intercom can receive/send transmittions (ACE3 interaction menu) but they cannot manipulate the radio (GUI interface)
            };
        };
	};

    class pvm_ffp_rg32m_gmg: ffp_rg32m_gmg {
        displayName="[PB] RG32m (GMG)";
        class AcreIntercoms {
            class Intercom_1 {
                displayName = "Crew intercom";             // Name of the intercom network displayed to the players
                shortName = "Crew";                        // Short name of the intercom network. Maximum of 5 characters
                // Seats with stations configured that have intercom access. In this case, units in commander, driver, gunner and turret (excluding FFV) have access to this intercom
                // If left empty it has the same effect
                allowedPositions[] = {"all"};
                // In this case the commander turret does not have access to crew intercom (unit is "turned out"). This can be useful for historical vehicles (default: {})
                disabledPositions[] = {};
                // Despite not having regular access to the network, units in cargo positions can have limited connections to communicate with the crew. These positions do not transmit automatically in the limited network; units in this position must toggle the functionality manually. (default: {})
                limitedPositions[] = {};
                // This is the number of simultaneous connections that units defined in the previous array can have (default: 0)
                numLimitedPositions = 0;
                // Seats with master stations have the possibility of broadcasting a message in that network (default: {})
                masterPositions[] = {};
                // The intercom initial configuration is enabled upon entering a vehicle (default: 0)
                connectedByDefault = 1;
            };
        };

        class AcreRacks {
            class Rack_1 {
                displayName = "Dashboard Upper";      // Name displayed in the interaction menu
                shortName = "D.Up";                   // Short name displayed on the HUD. Maximum of 5 characters
                componentName = "ACRE_VRC110";        // Able to mount a PRC152
                allowedPositions[] = {"driver", {"cargo", 1}}; // Who can configure the radio and open the radio GUI. Same wildcards as the intercom. It also allows transmitting/receiving
                disabledPositions[] = {};             // Who cannot access the radio (default: {})
                defaultComponents[] = {};             // Use this to attach simple components like Antennas, they will first attempt to fill empty connectors but will overide existing connectors. Not yet fully implemented. (default: {})
                mountedRadio = "";                    // Predefined mounted radio (default: "", meaning none)
                isRadioRemovable = 1;                 // Radio can be removed (default: 0)
                intercom[] = {};                      // Radio not wired to any intercom. All units in intercom can receive/send transmittions (ACE3 interaction menu) but they cannot manipulate the radio (GUI interface) (default: {})
            };
            class Rack_2 {
                displayName = "Dashboard Lower";      // Name displayed in the interaction menu
                shortName = "D.Low";                  // Short name displayed on the HUD. Maximum of 5 characters
                componentName = "ACRE_VRC103";        // Rack type (able to mount a PRC117F)
                allowedPositions[] = {"driver", "commander", "gunner"}; // Who can configure the radio and open the radio GUI. Same wildcards as the intercom. It also allows transmitting/receiving
                disabledPositions[] = {};
                defaultComponents[] = {};
                mountedRadio = "ACRE_PRC117F";        // Predefined mounted radio
                isRadioRemovable = 0;                 // Radio cannot be removed
                intercom[] = {};                      // Radio not wired to any intercom. All units in intercom can receive/send transmittions (ACE3 interaction menu) but they cannot manipulate the radio (GUI interface)
            };
        };
    };

    class Redd_Tank_Fuchs_1A4_Jg_Flecktarn;

    class PVM_Patria_XA_180_Series: Redd_Tank_Fuchs_1A4_Jg_Flecktarn {
        displayName = "[PB] Sisu XA-200";
        editorPreview="\Redd_Tank_Fuchs_1A4\pictures\F_JG_W.paa";
        scope=2;
        scopeCurator = 2;
        author="Redd, Tank, flaver, Semper";
        hiddenSelectionsTextures[] = {
            QPATHTOF(data\textures\Redd_Tank_Fuchs_1A4_Wanne_blend_co.paa),
            QPATHTOF(data\textures\Redd_Tank_Fuchs_1A4_Anbauteile_blend_co.paa),

            "\Redd_Tank_Fuchs_1A4\data\Redd_Tank_Fuchs_1A4_Reifen_blend_co.paa"
        };

       class AcreRacks {
            class Rack_1 {
                displayName = "Dashboard Upper";      // Name displayed in the interaction menu
                shortName = "D.Up";                   // Short name displayed on the HUD. Maximum of 5 characters
                componentName = "ACRE_VRC110";        // Able to mount a PRC152
                allowedPositions[] = {"driver", {"cargo", 1}}; // Who can configure the radio and open the radio GUI. Same wildcards as the intercom. It also allows transmitting/receiving
                disabledPositions[] = {};             // Who cannot access the radio (default: {})
                defaultComponents[] = {};             // Use this to attach simple components like Antennas, they will first attempt to fill empty connectors but will overide existing connectors. Not yet fully implemented. (default: {})
                mountedRadio = "";                    // Predefined mounted radio (default: "", meaning none)
                isRadioRemovable = 1;                 // Radio can be removed (default: 0)
                intercom[] = {};                      // Radio not wired to any intercom. All units in intercom can receive/send transmittions (ACE3 interaction menu) but they cannot manipulate the radio (GUI interface) (default: {})
            };
            class Rack_2 {
                displayName = "Dashboard Lower";      // Name displayed in the interaction menu
                shortName = "D.Low";                  // Short name displayed on the HUD. Maximum of 5 characters
                componentName = "ACRE_VRC103";        // Rack type (able to mount a PRC117F)
                allowedPositions[] = {"driver", "commander", "gunner"}; // Who can configure the radio and open the radio GUI. Same wildcards as the intercom. It also allows transmitting/receiving
                disabledPositions[] = {};
                defaultComponents[] = {};
                mountedRadio = "ACRE_PRC117F";        // Predefined mounted radio
                isRadioRemovable = 0;                 // Radio cannot be removed
                intercom[] = {};                      // Radio not wired to any intercom. All units in intercom can receive/send transmittions (ACE3 interaction menu) but they cannot manipulate the radio (GUI interface)
            };
        };
    };

    class pvm_PMPV_6X6_MiSu_gmg: rhsusf_M1232_MK19_usarmy_wd {
        displayName = "[PB] PMPV 6×6 MiSu (GMG)";
        HiddenSelectionsTextures[] = {
            QPATHTOF(data\textures\RG33_Body_WD_CO.paa),
            "rhsusf\addons\rhsusf_RG33L\data\RG33_Chassis_WD_CO.paa",
            "rhsusf\addons\rhsusf_RG33L\data\RG33_Accessory2_WD_CO.paa",
            "rhsusf\addons\rhsusf_RG33L\data\RG33_Armor_WD_CO.paa",
            QPATHTOF(data\textures\RG33_TurretWD_CO.paa),
            "rhsusf\addons\rhsusf_RG33L\data\RG33_Wheels_WD_CO.paa",
            "rhsusf\addons\rhsusf_hmmwv\textures\mk64mount_w_co.paa",
            "rhsusf\addons\rhsusf_RG33L\data\MCTAGS_CO.paa",
            "rhsusf\addons\rhsusf_rg33l\Data\rhsusf_camonet_wdl_co.paa",
            QPATHTOF(data\textures\RG33_USARMY_Decal_ca.paa)
        };
    };

    class pvm_PMPV_6X6_MiSu_m2 :rhsusf_M1232_M2_usarmy_wd {
        displayName = "[PB] PMPV 6×6 MiSu (M2)";
        HiddenSelectionsTextures[] = {
            QPATHTOF(data\textures\RG33_Body_WD_CO.paa),
            "rhsusf\addons\rhsusf_RG33L\data\RG33_Chassis_WD_CO.paa",
            "rhsusf\addons\rhsusf_RG33L\data\RG33_Accessory2_WD_CO.paa",
            "rhsusf\addons\rhsusf_RG33L\data\RG33_Armor_WD_CO.paa",
            QPATHTOF(data\textures\RG33_TurretWD_CO.paa),
            "rhsusf\addons\rhsusf_RG33L\data\RG33_Wheels_WD_CO.paa",
            "rhsusf\addons\rhsusf_hmmwv\textures\mk64mount_w_co.paa",
            "rhsusf\addons\rhsusf_RG33L\data\MCTAGS_CO.paa",
            "rhsusf\addons\rhsusf_rg33l\Data\rhsusf_camonet_wdl_co.paa",
            QPATHTOF(data\textures\RG33_USARMY_Decal_ca.paa)
        };
    };

};
