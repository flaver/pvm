## Pori Brigade Extended Veteran Mod
This mod is a Comp and a Extension Addon, for the amazing Veteran Mod.

## Uniforms
- FDF Uniforms <img src="https://img.shields.io/badge/Type-Comp-green.svg">
- CUP USMC Uniforms <img src="https://img.shields.io/badge/Type-Comp-green.svg">
- CUP BAF Uniforms <img src="https://img.shields.io/badge/Type-Comp-green.svg">
- VSM Uniforms <img src="https://img.shields.io/badge/Type-Comp-green.svg">
- Gendamerie Uniforms <img src="https://img.shields.io/badge/Type-Comp-green.svg">

## Vests
- FDF Vets <img src="https://img.shields.io/badge/Type-Comp-green.svg">
- CUP USMC Vests <img src="https://img.shields.io/badge/Type-Comp-green.svg">
- CUP BAF Vests <img src="https://img.shields.io/badge/Type-Comp-green.svg">
- VSM Vests <img src="https://img.shields.io/badge/Type-Comp-green.svg">
- Gendamerie Vests <img src="https://img.shields.io/badge/Type-Comp-green.svg">

## Functions
- Holster Weapon <img src="https://img.shields.io/badge/Type-Created-orange.svg">
- Interaction Menu <img src="https://img.shields.io/badge/Type-Created-orange.svg">