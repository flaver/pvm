class CfgVehicles {

	class Bag_Base;

	class PVM_BASE_Bag: Bag_Base {
		author = "3CB, Fingolfin & flaver";
		scope = 1;
		weaponPoolAvailable = 1;
		isbackpack = 1;
		transportMaxWeapons = 2;
		transportMaxMagazines = 40;
		maximumLoad = 220;
		mass = 70;
		class TransportMagazines {
		};
	};

	class PVM_Engineer_Bag: PVM_BASE_Bag {
		scope = 2;
		displayName="[PB] Engineer Backpack (M05)";
		model="pvm_backpacks\engineer.p3d";
		hiddenSelections[] = {"Camo1", "Camo2", "Camo3"};
		hiddenSelectionsTextures[] = {
			"pvm_backpacks\data\gear_tan_co.paa", 
			"pvm_backpacks\data\backpack_us_olive_co.paa", 
			"pvm_backpacks\data\jackal_adds_co.paa"
		};
	};
};